package com.ycagri.challenge.ui.domain.model;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.ycagri.challenge.database.DeliveryDatabaseContract;

/**
 * Plain old Java object designed as model to keep information about delivery items retrieved from
 * either remote or local. Uses {@link Gson} to deserialize {@link com.google.gson.JsonElement}
 * which is response of the remote request.
 */
public class Delivery implements Parcelable {

    @SerializedName("id")
    private final int mId;

    @SerializedName("description")
    private final String mDescription;

    @SerializedName("imageUrl")
    private final String mImageUrl;

    @SerializedName("location")
    private final DeliveryLocation mLocation;

    /**
     * Constructor used for items retrieved from local database
     *
     * @param id          Id of the delivery
     * @param description Keeps information about delivery
     * @param imageUrl    Url of the thumbnail of the delivery
     * @param latitude    Latitude of the delivery
     * @param longitude   Longitude of the delivery
     * @param address     Address text of the delivery
     */
    public Delivery(int id, String description, String imageUrl, double latitude, double longitude, String address) {
        mId = id;
        mDescription = description;
        mImageUrl = imageUrl;
        mLocation = new DeliveryLocation(latitude, longitude, address);
    }

    /**
     * Default constructor for {@link Parcelable} interface
     *
     * @param in Parcel to reconstruct object
     */
    private Delivery(Parcel in) {
        mId = in.readInt();
        mDescription = in.readString();
        mImageUrl = in.readString();
        mLocation = in.readParcelable(DeliveryLocation.class.getClassLoader());
    }

    public int getId() {
        return mId;
    }

    public String getDescription() {
        return mDescription;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public DeliveryLocation getLocation() {
        return mLocation;
    }

    /**
     * Converts delivery object to {@link ContentValues}
     *
     * @return {@link ContentValues} contains object members to insert database
     */
    public ContentValues toContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(DeliveryDatabaseContract.DeliveryEntry.COLUMN_ID, mId);
        cv.put(DeliveryDatabaseContract.DeliveryEntry.COLUMN_DESCRIPTION, mDescription);
        cv.put(DeliveryDatabaseContract.DeliveryEntry.COLUMN_IMAGE, mImageUrl);
        cv.put(DeliveryDatabaseContract.DeliveryEntry.COLUMN_LATITUDE, mLocation.getLatitude());
        cv.put(DeliveryDatabaseContract.DeliveryEntry.COLUMN_LONGITUDE, mLocation.getLongitude());
        cv.put(DeliveryDatabaseContract.DeliveryEntry.COLUMN_ADDRESS, mLocation.getAddress());
        return cv;
    }

    public static final Creator<Delivery> CREATOR = new Creator<Delivery>() {
        @Override
        public Delivery createFromParcel(Parcel in) {
            return new Delivery(in);
        }

        @Override
        public Delivery[] newArray(int size) {
            return new Delivery[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mId);
        dest.writeString(mDescription);
        dest.writeString(mImageUrl);
        dest.writeParcelable(mLocation, flags);
    }

    /**
     * Plain old Java object keeps information about {@link Delivery} location
     */
    public static class DeliveryLocation implements Parcelable {

        @SerializedName("lat")
        private final double mLatitude;

        @SerializedName("lng")
        private final double mLongitude;

        @SerializedName("address")
        private final String mAddress;

        DeliveryLocation(double latitude, double longitude, String address) {
            mLatitude = latitude;
            mLongitude = longitude;
            mAddress = address;
        }

        DeliveryLocation(Parcel in) {
            mLatitude = in.readDouble();
            mLongitude = in.readDouble();
            mAddress = in.readString();
        }

        public double getLatitude() {
            return mLatitude;
        }

        public double getLongitude() {
            return mLongitude;
        }

        public String getAddress() {
            return mAddress;
        }

        public static final Creator<DeliveryLocation> CREATOR = new Creator<DeliveryLocation>() {
            @Override
            public DeliveryLocation createFromParcel(Parcel in) {
                return new DeliveryLocation(in);
            }

            @Override
            public DeliveryLocation[] newArray(int size) {
                return new DeliveryLocation[size];
            }
        };

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeDouble(mLatitude);
            dest.writeDouble(mLongitude);
            dest.writeString(mAddress);
        }
    }
}
