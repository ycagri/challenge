package com.ycagri.challenge.ui.domain.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;

import com.ycagri.challenge.ui.domain.model.Delivery;
import com.ycagri.challenge.ui.domain.navigation.DeliveryItemNavigator;

import java.lang.ref.WeakReference;

import static dagger.internal.Preconditions.checkNotNull;

public class DeliveryItemViewModel extends BaseObservable {

    private final Delivery mDelivery;

    private final WeakReference<DeliveryItemNavigator> mNavigator;

    public DeliveryItemViewModel(@NonNull Delivery delivery,
                                 @NonNull DeliveryItemNavigator navigator) {
        mDelivery = checkNotNull(delivery);
        mNavigator = new WeakReference<>(navigator);
    }

    @Bindable
    public String getImageUrl() {
        return mDelivery.getImageUrl();
    }

    @Bindable
    public String getDescription() {
        return mDelivery.getDescription();
    }

    public void onClick() {
        mNavigator.get().onDeliverySelected(mDelivery);
    }
}
