package com.ycagri.challenge.ui.domain.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.NonNull;

import com.ycagri.challenge.ui.domain.model.Delivery;

import javax.inject.Inject;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * View model of the view {@link com.ycagri.challenge.ui.DetailFragment}. Simply performs IO operations
 * and view updates using databinding.
 *
 * @author ycagri
 * @since 16.09.2018
 */
public class DetailFragmentViewModel extends BaseObservable {

    /**
     * Selected delivery which is shown on screen for details
     */
    private final Delivery mDelivery;

    @Inject
    DetailFragmentViewModel(@NonNull Delivery delivery) {
        mDelivery = checkNotNull(delivery);
    }

    @Bindable
    public String getImageUrl() {
        return mDelivery.getImageUrl();
    }

    @Bindable
    public String getDescription() {
        return mDelivery.getDescription();
    }

    public Delivery getDelivery() {
        return mDelivery;
    }
}
