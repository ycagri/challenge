package com.ycagri.challenge.ui;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ycagri.challenge.R;
import com.ycagri.challenge.databinding.FragmentMasterBinding;
import com.ycagri.challenge.databinding.ItemDeliveryBinding;
import com.ycagri.challenge.di.ActivityScoped;
import com.ycagri.challenge.ui.domain.model.Delivery;
import com.ycagri.challenge.ui.domain.navigation.DeliveryItemNavigator;
import com.ycagri.challenge.ui.domain.navigation.MasterFragmentNavigator;
import com.ycagri.challenge.ui.domain.viewmodel.DeliveryItemViewModel;
import com.ycagri.challenge.ui.domain.viewmodel.MasterViewModel;
import com.ycagri.challenge.utils.ActivityUtils;
import com.ycagri.challenge.utils.BindingRecyclerAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

/**
 * Implements master part of the master-detail design and view part of MVVM. Simply contains a
 * {@link RecyclerView} to show items which are retrieved from either remote or local.
 *
 * @author ycagri
 * @since 16.09.2018
 */
@ActivityScoped
public class MasterFragment extends DaggerFragment implements MasterFragmentNavigator {

    /**
     * View model handles ui and background operations
     */
    @Inject
    MasterViewModel mViewModel;

    public static MasterFragment newInstance() {
        return new MasterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentMasterBinding binding = FragmentMasterBinding.inflate(inflater, container, false);
        binding.setViewModel(mViewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        FragmentMasterBinding binding = DataBindingUtil.getBinding(view);
        if (binding != null) {
            binding.rvDeliveries.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
            binding.rvDeliveries.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));
            binding.rvDeliveries.setHasFixedSize(true);
            binding.rvDeliveries.setAdapter(new DeliveryAdapter(new ArrayList<>()));
        }

        mViewModel.getDeliveriesFromRemote();
    }

    @Override
    public void onDeliveriesRetrieved(List<Delivery> deliveries, boolean firstPage) {
        if (deliveries.isEmpty())
            Toast.makeText(getContext(), R.string.error_empty_deliveries, Toast.LENGTH_SHORT).show();
        else {
            mViewModel.insertDeliveriesLocalDatabase(deliveries);
            if (getActivity() != null && getActivity().getResources().getBoolean(R.bool.multipane) && firstPage)
                ActivityUtils.addFragmentToActivity(getActivity().getSupportFragmentManager(),
                        DetailFragment.newInstance(deliveries.get(0)), R.id.detail_container, false);
        }
    }

    @Override
    public void onRemoteRequestError() {
        mViewModel.getDeliveriesFromLocal();

        Toast.makeText(getContext(), R.string.error_remote_request, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocalError() {
        Toast.makeText(getContext(), R.string.error_local_request, Toast.LENGTH_SHORT).show();
    }

    /**
     * Adapter designed to show {@link Delivery} items in {@link RecyclerView}.
     */
    final class DeliveryAdapter extends BindingRecyclerAdapter<Delivery, DeliveryAdapter.DeliveryViewHolder>
            implements DeliveryItemNavigator {

        DeliveryAdapter(List<Delivery> items) {
            super(items);
        }

        @NonNull
        @Override
        public DeliveryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            return new DeliveryViewHolder(ItemDeliveryBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false).getRoot());
        }

        @Override
        public void onBindViewHolder(@NonNull DeliveryViewHolder holder, int position) {
            ItemDeliveryBinding binding = DataBindingUtil.getBinding(holder.itemView);
            if (binding != null)
                binding.setViewModel(new DeliveryItemViewModel(mItems.get(position), this));
        }

        @Override
        public void onDeliverySelected(Delivery delivery) {
            if (getActivity() != null) {
                if (getActivity().getResources().getBoolean(R.bool.multipane))
                    ActivityUtils.addFragmentToActivity(getActivity().getSupportFragmentManager(),
                            DetailFragment.newInstance(delivery), R.id.detail_container, false);
                else
                    ActivityUtils.addFragmentToActivity(getActivity().getSupportFragmentManager(),
                            DetailFragment.newInstance(delivery), R.id.fragment_container, true);
            }
        }

        final class DeliveryViewHolder extends RecyclerView.ViewHolder {

            DeliveryViewHolder(View itemView) {
                super(itemView);
            }
        }
    }
}
