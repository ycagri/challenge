package com.ycagri.challenge.ui.domain.navigation;

import com.ycagri.challenge.ui.domain.model.Delivery;

/**
 * Provides communication between view {@link com.ycagri.challenge.ui.MasterFragment.DeliveryAdapter}
 * and view model {@link com.ycagri.challenge.ui.domain.viewmodel.DeliveryItemViewModel}.
 *
 * @author ycagri
 * @since 16.09.2018
 */
public interface DeliveryItemNavigator {

    void onDeliverySelected(Delivery delivery);
}
