package com.ycagri.challenge.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ycagri.challenge.R;
import com.ycagri.challenge.databinding.FragmentDetailBinding;
import com.ycagri.challenge.di.FragmentScoped;
import com.ycagri.challenge.ui.domain.model.Delivery;
import com.ycagri.challenge.ui.domain.viewmodel.DetailFragmentViewModel;

import javax.inject.Inject;

import dagger.android.support.DaggerFragment;

/**
 * Implements detail part of the master-detail design and view part of MVVM. Simply shows details of
 * selected item in {@link MasterFragment}. Contains {@link SupportMapFragment} to display location
 * of the item.
 *
 * @author ycagri
 * @since 16.09.2018
 */
@FragmentScoped
public class DetailFragment extends DaggerFragment implements OnMapReadyCallback {

    private static final String KEY_DELIVERY = "delivery";

    @Inject
    DetailFragmentViewModel mViewModel;

    public static DetailFragment newInstance(Delivery delivery) {
        Bundle args = new Bundle();
        args.putParcelable(KEY_DELIVERY, delivery);

        DetailFragment fragment = new DetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentDetailBinding binding = FragmentDetailBinding.inflate(inflater, container, false);
        binding.setViewModel(mViewModel);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public Delivery getDelivery() {
        return getArguments() != null ? getArguments().getParcelable(KEY_DELIVERY) : null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Delivery.DeliveryLocation location = mViewModel.getDelivery().getLocation();
        LatLng position = new LatLng(location.getLatitude(), location.getLongitude());
        googleMap.addMarker(new MarkerOptions()
                .title(location.getAddress())
                .position(position));

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 14));
    }
}
