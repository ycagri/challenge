package com.ycagri.challenge.ui;

import android.os.Bundle;

import com.ycagri.challenge.R;
import com.ycagri.challenge.di.ActivityScoped;
import com.ycagri.challenge.utils.ActivityUtils;

import dagger.android.support.DaggerAppCompatActivity;

/**
 * {@link android.app.Activity} that defines entry point for the application. Extends
 * {@link DaggerAppCompatActivity} in order to use Dagger style dependency injection. Simply implements
 * master-detail flow.
 *
 * @author ycagri
 * @since 16.09.2018
 */
@ActivityScoped
public class MainActivity extends DaggerAppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ActivityUtils.replaceFragmentToActivity(getSupportFragmentManager(), MasterFragment.newInstance(), R.id.fragment_container);
    }
}
