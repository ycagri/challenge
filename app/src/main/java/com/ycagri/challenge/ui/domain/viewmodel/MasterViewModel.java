package com.ycagri.challenge.ui.domain.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.ycagri.challenge.datasource.ChallengeRepository;
import com.ycagri.challenge.ui.domain.model.Delivery;
import com.ycagri.challenge.ui.domain.navigation.MasterFragmentNavigator;
import com.ycagri.challenge.utils.EspressoIdlingResource;

import java.lang.ref.WeakReference;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;

import static dagger.internal.Preconditions.checkNotNull;

/**
 * View model of the view {@link com.ycagri.challenge.ui.MasterFragment}. Simply performs IO operations
 * and view updates using databinding and navigator {@link MasterFragmentNavigator}.
 *
 * @author ycagri
 * @since 16.09.2018
 */
public class MasterViewModel extends BaseObservable {

    private static final int LIMIT = 20;

    private final ChallengeRepository mRepository;

    private final WeakReference<MasterFragmentNavigator> mNavigator;

    private final Scheduler mProcessScheduler;

    private final Scheduler mAndroidScheduler;

    /**
     * Keeps offset for remote requests
     */
    private int mRemoteOffset = 0;

    /**
     * Keeps offset for local database queries
     */
    private int mLocalOffset = 0;

    public ObservableList<Delivery> deliveries = new ObservableArrayList<>();

    @Bindable
    public RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (!recyclerView.canScrollVertically(1))
                getDeliveriesFromRemote();
        }
    };

    @Inject
    MasterViewModel(@NonNull ChallengeRepository repository,
                    @NonNull MasterFragmentNavigator navigator,
                    @NonNull @Named("process") Scheduler processScheduler,
                    @NonNull @Named("android") Scheduler androidScheduler) {
        mRepository = checkNotNull(repository);
        mNavigator = new WeakReference<>(checkNotNull(navigator));
        mProcessScheduler = checkNotNull(processScheduler);
        mAndroidScheduler = checkNotNull(androidScheduler);
    }

    public void getDeliveriesFromRemote() {
        EspressoIdlingResource.increment();
        mRepository.retrieveDeliveries(mRemoteOffset, LIMIT)
                .subscribeOn(mProcessScheduler)
                .observeOn(mAndroidScheduler)
                .doFinally(() -> {
                    if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
                        EspressoIdlingResource.decrement(); // Set app as idle.
                    }
                })
                .subscribe(new SingleObserver<List<Delivery>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(List<Delivery> deliveryList) {
                        boolean firstPage = mRemoteOffset == 0;
                        mLocalOffset = 0;
                        deliveries.addAll(deliveryList);
                        mRemoteOffset += LIMIT;

                        if (mNavigator.get() != null)
                            mNavigator.get().onDeliveriesRetrieved(deliveryList, firstPage);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mNavigator.get() != null)
                            mNavigator.get().onRemoteRequestError();
                    }
                });
    }

    public void insertDeliveriesLocalDatabase(List<Delivery> deliveryList) {
        mRepository.insertDeliveries(deliveryList, mRemoteOffset == 0)
                .subscribeOn(mProcessScheduler)
                .subscribe();
    }

    public void getDeliveriesFromLocal() {
        EspressoIdlingResource.increment();
        mRepository.retrieveLocalDeliveries(mLocalOffset, LIMIT)
                .take(1)
                .subscribeOn(mProcessScheduler)
                .observeOn(mAndroidScheduler)
                .doFinally(() -> {
                    if (!EspressoIdlingResource.getIdlingResource().isIdleNow()) {
                        EspressoIdlingResource.decrement(); // Set app as idle.
                    }
                })
                .subscribe(new Observer<List<Delivery>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Delivery> deliveryList) {
                        boolean firstPage = mLocalOffset == 0;
                        deliveries.addAll(deliveryList);
                        mLocalOffset += LIMIT;

                        if (mNavigator.get() != null)
                            mNavigator.get().onDeliveriesRetrieved(deliveryList, firstPage);
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (mNavigator.get() != null)
                            mNavigator.get().onLocalError();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
