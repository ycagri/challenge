package com.ycagri.challenge.ui.domain.navigation;

import com.ycagri.challenge.ui.domain.model.Delivery;

import java.util.List;

/**
 * Provides communication between view {@link com.ycagri.challenge.ui.MasterFragment}
 * and view model {@link com.ycagri.challenge.ui.domain.viewmodel.MasterViewModel}.
 *
 * @author ycagri
 * @since 16.09.2018
 */
public interface MasterFragmentNavigator {

    void onDeliveriesRetrieved(List<Delivery> deliveries, boolean firstPage);

    void onRemoteRequestError();

    void onLocalError();
}
