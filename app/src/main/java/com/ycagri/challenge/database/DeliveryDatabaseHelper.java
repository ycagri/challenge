package com.ycagri.challenge.database;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DeliveryDatabaseHelper extends SQLiteOpenHelper {
    public DeliveryDatabaseHelper(Context context, SQLiteDatabase.CursorFactory factory) {
        super(context, DeliveryDatabaseContract.DB_NAME, factory, DeliveryDatabaseContract.DB_VERSION);
    }

    public DeliveryDatabaseHelper(Context context, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
        super(context, DeliveryDatabaseContract.DB_NAME, factory, DeliveryDatabaseContract.DB_VERSION, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DeliveryDatabaseContract.DeliveryEntry.CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DeliveryDatabaseContract.DeliveryEntry.DROP_TABLE);
        db.execSQL(DeliveryDatabaseContract.DeliveryEntry.CREATE_TABLE);
    }
}
