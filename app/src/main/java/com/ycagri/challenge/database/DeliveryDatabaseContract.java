package com.ycagri.challenge.database;

import android.provider.BaseColumns;

/**
 * Contract class defines delivery database tables and their structure.
 *
 * @author ycagri
 * @since 16.09.2018
 */

public class DeliveryDatabaseContract {

    public static final String DB_NAME = "db_deliveries";
    public static final int DB_VERSION = 1;

    // To prevent someone from accidentally instantiating the contract class,
    // give it an empty constructor.
    public DeliveryDatabaseContract() {
    }

    public static abstract class DeliveryEntry implements BaseColumns {

        public static final String TABLE_NAME = "tbl_deliveries";
        public static final String COLUMN_ID = "id";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_IMAGE = "image_url";
        public static final String COLUMN_LONGITUDE = "longitude";
        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_ADDRESS = "address";

        public static final String CREATE_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY NOT NULL, " +
                COLUMN_DESCRIPTION + " TEXT, " +
                COLUMN_IMAGE + " TEXT, " +
                COLUMN_LONGITUDE + " DOUBLE, " +
                COLUMN_LATITUDE + " DOUBLE, " +
                COLUMN_ADDRESS + " TEXT);";

        public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME + ";";
    }
}
