package com.ycagri.challenge.di;

import com.ycagri.challenge.ui.DetailFragment;
import com.ycagri.challenge.ui.DetailFragmentModule;
import com.ycagri.challenge.ui.MasterFragment;
import com.ycagri.challenge.ui.MasterFragmentModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Defines fragment dependency injections using by Dagger.
 *
 * @author ycagri
 * @since 16.09.2018
 */
@Module
abstract class FragmentBindingModule {

    @FragmentScoped
    @ContributesAndroidInjector(modules = MasterFragmentModule.class)
    public abstract MasterFragment masterFragment();

    @FragmentScoped
    @ContributesAndroidInjector(modules = DetailFragmentModule.class)
    public abstract DetailFragment detailFragment();
}
