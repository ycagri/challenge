package com.ycagri.challenge.core;

import android.app.Application;

import com.google.gson.GsonBuilder;
import com.ycagri.challenge.di.AppComponent;
import com.ycagri.challenge.di.DaggerAppComponent;
import com.ycagri.challenge.utils.RetrofitApiInterface;

import java.util.concurrent.TimeUnit;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * We create a custom {@link Application} class that extends  {@link DaggerApplication}.
 * We then override applicationInjector() which tells Dagger how to make our @Singleton Component
 * We never have to call `component.inject(this)` as {@link DaggerApplication} will do that for us.
 *
 * @author ycagri
 * @since 16.09.2018
 */
public class ChallengeApplication extends DaggerApplication {

    public static final String SERVER_URL = "http://10.10.1.47:8080";

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(logging)
                .hostnameVerifier((s, sslSession) -> true)
                .build();

        AppComponent appComponent = DaggerAppComponent.builder()
                .application(this)
                .scheduler(Schedulers.io())
                .pitcherRetrofitApi(new Retrofit.Builder().baseUrl(SERVER_URL)
                        .client(okHttpClient)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(ScalarsConverterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                        .build()
                        .create(RetrofitApiInterface.class))
                .build();
        appComponent.inject(this);
        return appComponent;
    }
}
