package com.ycagri.challenge.utils;

import android.support.v7.widget.RecyclerView;

import java.util.List;

import static dagger.internal.Preconditions.checkNotNull;

public abstract class BindingRecyclerAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected List<T> mItems;

    public BindingRecyclerAdapter(List<T> items) {
        mItems = checkNotNull(items);
    }

    void replaceData(List<T> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }
}
