package com.ycagri.challenge.utils;

import android.databinding.BindingAdapter;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.List;

public class CommonBindingUtils {

    @SuppressWarnings("unchecked")
    @BindingAdapter("items")
    public static <T, VH extends RecyclerView.ViewHolder> void setItems(RecyclerView recyclerView, List<T> items) {
        BindingRecyclerAdapter<T, VH> adapter = (BindingRecyclerAdapter) recyclerView.getAdapter();
        if (adapter != null) {
            adapter.replaceData(items);
        }
    }

    @BindingAdapter("scrollListener")
    public static void addScrollListener(RecyclerView recyclerView, RecyclerView.OnScrollListener scrollListener) {
        recyclerView.addOnScrollListener(scrollListener);
    }

    @SuppressWarnings("unchecked")
    @BindingAdapter("url")
    public static void setUrl(ImageView imageView, String url) {
        Picasso.get().load(url).into(imageView);
    }
}
