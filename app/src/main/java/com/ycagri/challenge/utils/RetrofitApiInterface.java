package com.ycagri.challenge.utils;

import com.ycagri.challenge.ui.domain.model.Delivery;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitApiInterface {

    @GET("deliveries")
    Single<List<Delivery>> getDeliveries(@Query("offset") int offset,
                                         @Query("limit") int limit);
}
