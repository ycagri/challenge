package com.ycagri.challenge.datasource;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.VisibleForTesting;

import com.squareup.sqlbrite2.BriteDatabase;
import com.squareup.sqlbrite2.SqlBrite;
import com.ycagri.challenge.database.DeliveryDatabaseContract;
import com.ycagri.challenge.database.DeliveryDatabaseHelper;
import com.ycagri.challenge.ui.domain.model.Delivery;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

import static dagger.internal.Preconditions.checkNotNull;

@Singleton
public class LocalDataConnection implements LocalDataSource {

    @NonNull
    private final BriteDatabase mDatabaseHelper;

    @NonNull
    private Function<Cursor, Delivery> mDeliveryMapperFunction;

    @Inject
    LocalDataConnection(@NonNull Context context,
                        @NonNull Scheduler scheduler) {
        checkNotNull(context, "context cannot be null");
        checkNotNull(scheduler, "scheduleProvider cannot be null");
        DeliveryDatabaseHelper dbHelper = new DeliveryDatabaseHelper(context, null);
        SqlBrite sqlBrite = new SqlBrite.Builder().build();
        mDatabaseHelper = sqlBrite.wrapDatabaseHelper(dbHelper, scheduler);
        mDeliveryMapperFunction = this::deliveryMapperFunction;
    }

    @Override
    public Observable<Delivery> insertDeliveries(List<Delivery> deliveries, boolean delete) {
        if (delete)
            mDatabaseHelper.delete(DeliveryDatabaseContract.DeliveryEntry.TABLE_NAME, null);

        return Observable.using(mDatabaseHelper::newTransaction,
                transaction -> inTransactionInstanceUpdate(deliveries, transaction),
                BriteDatabase.Transaction::end);
    }

    @Override
    public Observable<List<Delivery>> retrieveLocalDeliveries(int offset, int limit) {
        String query = String.format(Locale.getDefault(),
                "SELECT * FROM %s LIMIT %d OFFSET %d",
                DeliveryDatabaseContract.DeliveryEntry.TABLE_NAME, limit, offset);
        return mDatabaseHelper.createQuery(DeliveryDatabaseContract.DeliveryEntry.TABLE_NAME, query)
                .mapToList(mDeliveryMapperFunction);
    }

    @VisibleForTesting
    public void cleanUp(){
        mDatabaseHelper.delete(DeliveryDatabaseContract.DeliveryEntry.TABLE_NAME, null);
    }

    @NonNull
    private Delivery deliveryMapperFunction(Cursor c) {
        return new Delivery(
                c.getInt(c.getColumnIndexOrThrow(DeliveryDatabaseContract.DeliveryEntry.COLUMN_ID)),
                c.getString(c.getColumnIndexOrThrow(DeliveryDatabaseContract.DeliveryEntry.COLUMN_DESCRIPTION)),
                c.getString(c.getColumnIndexOrThrow(DeliveryDatabaseContract.DeliveryEntry.COLUMN_IMAGE)),
                c.getDouble(c.getColumnIndexOrThrow(DeliveryDatabaseContract.DeliveryEntry.COLUMN_LATITUDE)),
                c.getDouble(c.getColumnIndexOrThrow(DeliveryDatabaseContract.DeliveryEntry.COLUMN_LONGITUDE)),
                c.getString(c.getColumnIndexOrThrow(DeliveryDatabaseContract.DeliveryEntry.COLUMN_ADDRESS))
        );
    }

    @NonNull
    private Observable<Delivery> inTransactionInstanceUpdate(@NonNull List<Delivery> deliveries,
                                                             @NonNull BriteDatabase.Transaction transaction) {
        checkNotNull(deliveries);
        checkNotNull(transaction);

        return Observable.fromArray(deliveries.toArray(new Delivery[deliveries.size()]))
                .doOnNext(delivery -> mDatabaseHelper.insert(DeliveryDatabaseContract.DeliveryEntry.TABLE_NAME,
                        delivery.toContentValues(), SQLiteDatabase.CONFLICT_REPLACE))
                .doOnComplete(transaction::markSuccessful);
    }
}
