package com.ycagri.challenge.datasource;

import com.ycagri.challenge.ui.domain.model.Delivery;

import java.util.List;

import io.reactivex.Observable;

public interface LocalDataSource {

    Observable<Delivery> insertDeliveries(List<Delivery> deliveries, boolean delete);

    Observable<List<Delivery>> retrieveLocalDeliveries(int offset, int limit);
}
