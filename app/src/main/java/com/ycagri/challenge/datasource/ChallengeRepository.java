package com.ycagri.challenge.datasource;

import android.support.annotation.NonNull;

import com.ycagri.challenge.ui.domain.model.Delivery;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;

import static dagger.internal.Preconditions.checkNotNull;

public class ChallengeRepository implements RemoteDataSource, LocalDataSource {

    private final RemoteDataSource mRemoteDataSource;

    private final LocalDataSource mLocalDataSource;

    @Inject
    ChallengeRepository(@NonNull RemoteDataSource remoteDataSource,
                        @NonNull LocalDataSource localDataSource) {
        mRemoteDataSource = checkNotNull(remoteDataSource);
        mLocalDataSource = checkNotNull(localDataSource);
    }

    @Override
    public Single<List<Delivery>> retrieveDeliveries(int offset, int limit) {
        return mRemoteDataSource.retrieveDeliveries(offset, limit);
    }

    @Override
    public Observable<Delivery> insertDeliveries(List<Delivery> deliveries, boolean delete) {
        return mLocalDataSource.insertDeliveries(deliveries, delete);
    }

    @Override
    public Observable<List<Delivery>> retrieveLocalDeliveries(int offset, int limit) {
        return mLocalDataSource.retrieveLocalDeliveries(offset, limit);
    }
}
