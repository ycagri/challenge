package com.ycagri.challenge.datasource;

import android.support.annotation.NonNull;

import com.ycagri.challenge.ui.domain.model.Delivery;
import com.ycagri.challenge.utils.RetrofitApiInterface;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

import static dagger.internal.Preconditions.checkNotNull;

public class RemoteDataConnection implements RemoteDataSource {

    private final RetrofitApiInterface mRetrofitApi;

    @Inject
    RemoteDataConnection(@NonNull RetrofitApiInterface retrofitApi) {
        mRetrofitApi = checkNotNull(retrofitApi);
    }

    @Override
    public Single<List<Delivery>> retrieveDeliveries(int offset, int limit) {
        return mRetrofitApi.getDeliveries(offset, limit);
    }
}
