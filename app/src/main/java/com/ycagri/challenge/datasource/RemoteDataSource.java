package com.ycagri.challenge.datasource;

import com.ycagri.challenge.ui.domain.model.Delivery;

import java.util.List;

import io.reactivex.Single;

public interface RemoteDataSource {

    Single<List<Delivery>> retrieveDeliveries(int offset, int limit);
}
