package com.ycagri.challenge.datasource;

import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import com.google.gson.GsonBuilder;
import com.ycagri.challenge.ui.domain.model.Delivery;
import com.ycagri.challenge.utils.RetrofitApiInterface;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import io.reactivex.observers.TestObserver;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class RemoteDataConnectionTest {

    private static final String SERVER_URL = "http://10.10.1.47:8080";

    private RemoteDataConnection mRemoteConnection;

    @Before
    public void setup() {
        mRemoteConnection = new RemoteDataConnection(new Retrofit.Builder().baseUrl(SERVER_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .build()
                .create(RetrofitApiInterface.class));
    }

    @Test
    public void remoteConnectionTest() {
        TestObserver<List<Delivery>> testObserver = new TestObserver<>();
        mRemoteConnection.retrieveDeliveries(0, 20)
                .subscribe(testObserver);

        testObserver.assertNoErrors();
        testObserver.assertValue(deliveries -> deliveries.size() == 20);
    }
}
