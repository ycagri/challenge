package com.ycagri.challenge.datasource;

import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SmallTest;
import android.support.test.runner.AndroidJUnit4;

import com.ycagri.challenge.ui.domain.model.Delivery;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.internal.schedulers.ImmediateThinScheduler;
import io.reactivex.observers.TestObserver;

@RunWith(AndroidJUnit4.class)
@SmallTest
public class LocalDataConnectionTest {

    private LocalDataConnection mDatabaseConnection;

    @Before
    public void setup() {
        mDatabaseConnection = new LocalDataConnection(InstrumentationRegistry.getTargetContext(), ImmediateThinScheduler.INSTANCE);
        mDatabaseConnection.cleanUp();
    }

    @Test
    public void databaseTest() {
        List<Delivery> deliveries = new ArrayList<>();
        Delivery delivery = new Delivery(15, "Test Description",
                "https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-8.jpeg",
                22.335538, 114.176169, "Mock Address");
        deliveries.add(delivery);

        mDatabaseConnection.insertDeliveries(deliveries, false)
                .subscribe(new TestObserver<>());

        TestObserver<List<Delivery>> testObserver = new TestObserver<>();
        mDatabaseConnection.retrieveLocalDeliveries(0, 20)
                .subscribe(testObserver);

        testObserver.assertNoErrors();
        testObserver.assertValueCount(1);
        testObserver.assertValue(deliveryList -> deliveryList.get(0).getId() == delivery.getId());
        testObserver.assertValue(deliveryList -> deliveryList.get(0).getDescription().equals(delivery.getDescription()));
        testObserver.assertValue(deliveryList -> deliveryList.get(0).getImageUrl().equals(delivery.getImageUrl()));
        testObserver.assertValue(deliveryList -> deliveryList.get(0).getLocation().getLatitude() == delivery.getLocation().getLatitude());
        testObserver.assertValue(deliveryList -> deliveryList.get(0).getLocation().getLongitude() == delivery.getLocation().getLongitude());
        testObserver.assertValue(deliveryList -> deliveryList.get(0).getLocation().getAddress().equals(delivery.getLocation().getAddress()));
    }

    @After
    public void cleanUp() {
        mDatabaseConnection.cleanUp();
    }
}
