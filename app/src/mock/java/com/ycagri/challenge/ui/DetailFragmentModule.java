package com.ycagri.challenge.ui;

import com.ycagri.challenge.di.FragmentScoped;
import com.ycagri.challenge.ui.domain.model.Delivery;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailFragmentModule {

    @FragmentScoped
    @Provides
    static Delivery provideDelivery(DetailFragment fragment) {
        return fragment.getDelivery();
    }
}
