package com.ycagri.challenge.ui;

import com.ycagri.challenge.di.FragmentScoped;
import com.ycagri.challenge.ui.domain.navigation.MasterFragmentNavigator;

import javax.inject.Named;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Module
public abstract class MasterFragmentModule {

    @FragmentScoped
    @Binds
    abstract MasterFragmentNavigator provideNavigator(MasterFragment fragment);

    @FragmentScoped
    @Provides
    @Named("process")
    static Scheduler provideProcessScheduler(MasterFragment fragment) {
        return Schedulers.io();
    }

    @FragmentScoped
    @Provides
    @Named("android")
    static Scheduler provideAndroidScheduler(MasterFragment fragment) {
        return AndroidSchedulers.mainThread();
    }
}
