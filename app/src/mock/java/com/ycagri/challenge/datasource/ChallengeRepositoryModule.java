package com.ycagri.challenge.datasource;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;

@Module
public abstract class ChallengeRepositoryModule {

    @Singleton
    @Binds
    abstract RemoteDataSource provideRemoteDataSource(RemoteDataConnection dataSource);

    @Singleton
    @Binds
    abstract LocalDataSource provideLocalDataSource(LocalDataConnection dataSource);
}
