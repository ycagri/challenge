package com.ycagri.challenge.ui.domain.model;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DeliveryTest {

    @Test
    public void deliveryLocation_Test() {
        Delivery.DeliveryLocation location = mock(Delivery.DeliveryLocation.class);
        when(location.getLatitude()).thenReturn(22.335538);
        when(location.getLongitude()).thenReturn(114.176169);
        when(location.getAddress()).thenReturn("Mock Address");

        assertEquals(location.getLatitude(), 22.335538);
        assertEquals(location.getLongitude(), 114.176169);
        assertEquals(location.getAddress(), "Mock Address");
    }

    @Test
    public void delivery_Test() {
        Delivery delivery = mock(Delivery.class);

        when(delivery.getId()).thenReturn(43);
        when(delivery.getDescription()).thenReturn("Mock Description");
        when(delivery.getImageUrl()).thenReturn("https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-8.jpeg");

        Delivery.DeliveryLocation location = mock(Delivery.DeliveryLocation.class);
        when(location.getLatitude()).thenReturn(22.335538);
        when(location.getLongitude()).thenReturn(114.176169);
        when(location.getAddress()).thenReturn("Mock Address");
        when(delivery.getLocation()).thenReturn(location);

        assertEquals(delivery.getId(), 43);
        assertEquals(delivery.getDescription(), "Mock Description");
        assertEquals(delivery.getImageUrl(), "https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-8.jpeg");
        assertEquals(delivery.getLocation().getLatitude(), 22.335538);
        assertEquals(delivery.getLocation().getLongitude(), 114.176169);
        assertEquals(delivery.getLocation().getAddress(), "Mock Address");
    }
}
