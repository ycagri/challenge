package com.ycagri.challenge.ui.domain.viewmodel;


import com.google.common.collect.Lists;
import com.ycagri.challenge.datasource.ChallengeRepository;
import com.ycagri.challenge.ui.domain.model.Delivery;
import com.ycagri.challenge.ui.domain.navigation.MasterFragmentNavigator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MasterViewModelTest {

    private List<Delivery> deliveries;

    @Mock
    private ChallengeRepository mRepository;

    @Mock
    private MasterFragmentNavigator mNavigator;

    private MasterViewModel mViewModel;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mViewModel = new MasterViewModel(mRepository, mNavigator, Schedulers.trampoline(), Schedulers.trampoline());
        deliveries = Lists.newArrayList(
                new Delivery(0, "Description 1", "https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-8.jpeg", 22.335538, 114.176169, "Address 1"),
                new Delivery(1, "Description 2", "https://s3-ap-southeast-1.amazonaws.com/lalamove-mock-api/images/pet-8.jpeg", 22.335538, 114.176169, "Address 2"));
    }

    @Test
    public void deliveryRetrievalTest() {
        when(mRepository.retrieveDeliveries(0, 20)).thenReturn(Single.just(deliveries));
        mViewModel.getDeliveriesFromRemote();

        verify(mNavigator).onDeliveriesRetrieved(deliveries, true);
    }

    @Test
    public void deliveryRetrievalErrorTest() {
        when(mRepository.retrieveDeliveries(0, 20)).thenReturn(Single.error(new Exception()));
        mViewModel.getDeliveriesFromRemote();

        verify(mNavigator).onRemoteRequestError();
    }

    @Test
    public void deliveryRetrievalLocal() {
        when(mRepository.retrieveLocalDeliveries(0, 20)).thenReturn(Observable.just(deliveries));
        mViewModel.getDeliveriesFromLocal();

        assertEquals(mViewModel.deliveries.size(), 2);
    }
}
